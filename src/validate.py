from cerberus import Validator
from config import MAX_SEATS


class ValidationError(Exception):
    pass


def validate(request_json, schema):
    v = Validator(schema)
    if not v.validate(request_json):
        raise ValidationError


def seat_validate(seatNo):
    if not 1 <= int(seatNo) <= MAX_SEATS:
        raise ValidationError
