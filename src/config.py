MAX_SEATS = 40

SCHEMA_UPDATE_TICKET_OPEN = {
    "seatNo": {"type": "integer", "allowed": list(range(1, MAX_SEATS+1))},
}

SCHEMA_UPDATE_TICKET_CLOSE = {
    "seatNo": {"type": "integer", "allowed": list(range(1, MAX_SEATS+1))},
    "personDetails": {
        "type": "dict",
        "schema": {
            "name": {"type": "string"},
            "age": {"type": "integer"},
        },
    }
}

SCHEMA_RESET_SERVER = {
    "AdminKey": {"type": "string"},
}

ALL_SCHEMA = {
    "SCHEMA_UPDATE_TICKET_OPEN": SCHEMA_UPDATE_TICKET_OPEN,
    "SCHEMA_UPDATE_TICKET_CLOSE": SCHEMA_UPDATE_TICKET_CLOSE,
    "SCHEMA_RESET_SERVER": SCHEMA_RESET_SERVER
}
