from service import app, mongo, MAX_SEATS
import unittest


class TestService(unittest.TestCase):

    def setUp(self):
        test_app = app
        test_app.config['TESTING'] = True
        test_app.config["MONGO_URI"] = "mongodb://localhost:27017/unittest_server"
        mongo.db.bus.insert_many(
            [
                {'seatNo': 1, 'personDetails': {'name': 'foo', 'age': 10}},
                {'seatNo': 2, 'personDetails': {'name': 'bar', 'age': 20}},
                {'seatNo': 3, 'personDetails': None},
                {'seatNo': 4, 'personDetails': None},
                {'seatNo': 5, 'personDetails': None},
            ]
        )
        self.app = test_app.test_client()

    def tearDown(self):
        mongo.db.bus.delete_many({})

    def test_not_found(self):
        response = self.app.get('/foo/bar')
        self.assertEqual(response.status_code, 404)

    def test_bad_request(self):
        response = self.app.post(
            '/update_ticket/open',
            json={"foo": "bar"}
        )
        self.assertEqual(response.status_code, 400)

    def test_get_ticket_all(self):
        response = self.app.get('/get_ticket/all')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(5, len(response.json))

    def test_get_ticket_close(self):
        response = self.app.get('/get_ticket/close')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(2, len(response.json))

    def test_get_ticket_open(self):
        response = self.app.get('/get_ticket/open')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(3, len(response.json))

    def test_get_ticket_seat(self):
        response = self.app.get('/get_ticket/1')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, len(response.json))
        del response.json[0]['_id']
        self.assertEqual(
            [{'seatNo': 1, 'personDetails': {'name': 'foo', 'age': 10}}],
            response.json
        )

    def test_person_details_close_seat(self):
        response = self.app.get('/person_details/2')
        self.assertEqual(response.status_code, 200)
        self.assertEqual({'name': 'bar', 'age': 20}, response.json)

    def test_person_details_open_seat(self):
        response = self.app.get('/person_details/5')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(None, response.json)

    def test_person_details_invalid_seat(self):
        response = self.app.get('/person_details/100')
        self.assertEqual(response.status_code, 400)

    def test_update_ticket_open(self):
        response = self.app.post(
            '/update_ticket/open',
            json={"seatNo": 1}
        )
        self.assertEqual(response.status_code, 201)
        response = self.app.get('/get_ticket/1')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, len(response.json))
        del response.json[0]['_id']
        self.assertEqual(
            [{'seatNo': 1, 'personDetails': None}],
            response.json
        )

    def test_update_ticket_open_invalid_post_data(self):
        response = self.app.post(
            '/update_ticket/open',
            json={"seatNo": 2077}
        )
        self.assertEqual(response.status_code, 400)

    def test_update_ticket_close(self):
        response = self.app.post(
            '/update_ticket/close',
            json={"seatNo": 5, "personDetails": {'name': 'foo', 'age': 99}}
        )
        self.assertEqual(response.status_code, 201)
        response = self.app.get('/get_ticket/5')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, len(response.json))
        del response.json[0]['_id']
        self.assertEqual(
            [{'seatNo': 5, 'personDetails': {'name': 'foo', 'age': 99}}],
            response.json
        )

    def test_update_ticket_close_invalid_post_data(self):
        response = self.app.post(
            '/update_ticket/close',
            json={"seatNo": 5, "personDetails": {'name': 'foo', 'age': 99, 'baz': '007'}}
        )
        self.assertEqual(response.status_code, 400)

    def test_reset_server_unauthorized(self):
        response = self.app.post(
            '/reset_server',
            json={"AdminKey": "bar"}
        )
        self.assertEqual(response.status_code, 403)

    def test_reset_server_authorized(self):
        response = self.app.post(
            '/reset_server',
            json={"AdminKey": "Naive-Authentication"}
        )
        self.assertEqual(response.status_code, 201)
        response = self.app.get('/get_ticket/close')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, len(response.json))

    def test_reset_server_invalid_post_data(self):
        response = self.app.post(
            '/reset_server',
            json={"foo": "bar"}
        )
        self.assertEqual(response.status_code, 400)


if __name__ == "__main__":
    unittest.main()
