from flask import Flask
from flask_pymongo import PyMongo
from config import ALL_SCHEMA

app = Flask(__name__)
app.secret_key = "secret key"
app.config["MONGO_URI"] = "mongodb://localhost:27017/booking_server"
app.config.update(ALL_SCHEMA)
mongo = PyMongo(app)