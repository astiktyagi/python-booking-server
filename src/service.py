from application import app, mongo
from bson.json_util import dumps
from functools import wraps
from flask import jsonify, request, Response
from validate import ValidationError, validate, seat_validate
from config import MAX_SEATS

VALID_UPDATES = ('open', 'close')
REQ_FIELDS = ('updateType', 'seatNo', 'personDetails')


def validate_schema(schema_name):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                validate(request.json, app.config[schema_name])
            except ValidationError:
                return bad_request('Post data validation failed')
            return f(*args, **kw)

        return wrapper

    return decorator


def validate_seat():
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                seat_validate(kw['seatNo'])
            except ValidationError:
                return bad_request('Seat Number not in range 1 - 40')
            return f(*args, **kw)

        return wrapper

    return decorator


@app.errorhandler(400)
def bad_request(error=None):
    message = {
        'status': 400,
        'message': error or 'Bad Request',
        'requestData': request.json
    }
    resp = jsonify(message)
    resp.status_code = 400
    return resp


@app.errorhandler(500)
def internal_server_error(error=None):
    message = {
        'status': 500,
        'message': 'Internal Server Error: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 500
    return resp


@app.route('/get_ticket/<identifier>', methods=['GET'])
def get_ticket(identifier):
    """

    :param string identifier: identifier to filter down the result from database
    :return: HTTP 200
    """
    #  HACK: Since we are constrained to one ticket per seat we can use them interchangeably
    try:
        if identifier == 'all':
            tickets = mongo.db.bus.find()
        elif identifier == 'close':
            tickets = mongo.db.bus.find({'personDetails': {'$ne': None}})
        elif identifier == 'open':
            tickets = mongo.db.bus.find({'personDetails': {'$eq': None}})
        else:
            tickets = mongo.db.bus.find({'seatNo': {'$eq': int(identifier)}})
        return Response(
            dumps(tickets),
            mimetype='application/json',
            status=200
        )
    except:
        return internal_server_error()


@app.route('/person_details/<seatNo>', methods=['GET'])
@validate_seat()
def person_details(seatNo):
    """

    :param string seatNo: seat number whose person details are needed
    :return: HTTP 200
    """
    try:
        tickets = mongo.db.bus.find({'seatNo': {'$eq': int(seatNo)}})
        return Response(
            dumps(tickets[0]['personDetails']),
            mimetype='application/json',
            status=200
        )
    except:
        return internal_server_error()


@app.route('/update_ticket/open', methods=['POST'])
@validate_schema('SCHEMA_UPDATE_TICKET_OPEN')
def update_ticket_open():
    """
    opens up a closed ticket from the server
    """

    try:
        _json = request.json
        mongo.db.bus.replace_one(
            {'seatNo': {'$eq': _json["seatNo"]}},
            {'seatNo': _json["seatNo"], 'personDetails': None}
        )

        resp = jsonify('Ticket open successfully!')
        resp.status_code = 201
        return resp

    except:
        return internal_server_error()


@app.route('/update_ticket/close', methods=['POST'])
@validate_schema('SCHEMA_UPDATE_TICKET_CLOSE')
def update_ticket_close():
    """
    closes an open ticket, throws bad request incase ticket is re booked
    """

    try:
        _json = request.json
        tickets = mongo.db.bus.find({'seatNo': {'$eq': _json["seatNo"]}})
        if not tickets[0]['personDetails']:
            mongo.db.bus.replace_one(
                {'seatNo': {'$eq': _json["seatNo"]}, 'personDetails': {'$eq': None}},
                {'seatNo': _json["seatNo"], 'personDetails': _json["personDetails"]}
            )

            resp = jsonify('Ticket closed successfully!')
            resp.status_code = 201
            return resp
        else:
            return bad_request('Ticket already booked, please try another seat no')
    except:
        return internal_server_error()


@app.route('/reset_server', methods=['POST'])
@validate_schema('SCHEMA_RESET_SERVER')
def reset_server():
    """
    Opens up all the tickets
    {
        "AdminKey": "Naive-Authentication"
    }

    """
    try:
        _json = request.json
        key = _json.pop("AdminKey", None)
        if key and key == 'Naive-Authentication':
            reset_db()
            resp = jsonify('Server Reset!')
            resp.status_code = 201
            return resp
        else:
            return Response(status=403)
    except:
        return internal_server_error()


def reset_db():
    mongo.db.bus.delete_many({})
    mongo.db.bus.insert_many(
        [{'seatNo': i, 'personDetails': None} for i in range(1, MAX_SEATS + 1)]
    )


def insert_fake_data():
    for i in range(1, 36, 2):
        mongo.db.bus.update(
            {'seatNo': {'$eq': i}},
            {'seatNo': i, 'personDetails': {'name': 'Name_' + str(i), 'age': i + 20}}
        )


if __name__ == "__main__":
    reset_db()
    insert_fake_data()
    app.run(host='0.0.0.0')
